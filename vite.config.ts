/**
 * @type {import('vite').UserConfig}
 */

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import eslintPlugin from 'vite-plugin-eslint';
// https://vitejs.dev/config/

const config = {
  plugins: [vue(), eslintPlugin()],
}

export default defineConfig(config)
